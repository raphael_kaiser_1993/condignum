# condignum

Requirements:
	- Python 3.7.3
	- Node.js v10.21.0
	- create-react-app 4.0.1

Installation/start:
	- Clone the repostitory to your local computer.
	- Open Linux terminal and go into condignum/question-answer-app/backend
	- Type in
		$ pipenv shell
		$ pipenv install django
		$ python manage.py makemigrations qa
		$ python manage.py migrate
		$ python manage.py runserver
	- In another terminal go into condignum/question-answer-app/frontend
	- Type in
		$ yarn start

Usage:
	- Questions can only be added or modified over backend interface
	  http://localhost:8000/api/qas
	- Answers are given over clicking the button with the text "Answer" on it
	- In popup windows the answer can be saved with pushing the Save-button
