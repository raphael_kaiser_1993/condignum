import React, { Component } from "react";
import Modal from "./components/Modal.js";
import axios from "axios";

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			viewCompleted: false,
			activeItem: {
				title: "",
				answer1: false,
				answer2: false,
				answer3: false,
			},
			qaList: []
		};
	}
	
	componentDidMount () {
		this.refreshList ();
	}
	
	refreshList = () => {
		axios
			.get ( "http://localhost:8000/api/qas" )
			.then ( res => this.setState ( { qaList: res.data } ) )
			.catch ( err => console.log ( err ) );
	};
	
	displayCompleted = status => {
		if (status) {
			return this.setState({ viewCompleted: true });
		}
		return this.setState({ viewCompleted: false });
	};
	
	renderTabList = () => {
		return (
			<div className="my-5 tab-list">
				<span
					onClick={() => this.displayCompleted(true)}
					className={this.state.viewCompleted ? "active" : ""}
				>
					complete
				</span>
				<span
					onClick={() => this.displayCompleted(false)}
					className={this.state.viewCompleted ? "" : "active"}
				>
					Incomplete
				</span>
			</div>
		);
	};
	
	renderItems = () => {
		/* I planned to implement a split view for completed and not completed
		 * iterations but I did not reach this point during development.
		 * Therefore is this draft useless at the moment.*/
		const { viewCompleted } = this.state;
		const newItems = this.state.qaList.filter(
			item => false == viewCompleted
		);
		
		return newItems.map(item => (
			<li
				key={item.id}
				className="list-group-item d-flex justify-content-between align-items-center"
			>
				<span
					className={`qa-title mr-2 ${
						this.state.viewCompleted ? "completed-qa" : ""
					}`}
				>
					{item.title}
				</span>
				<span>
					<button
						onClick = { () => this.editItem ( item ) }
						className = "btn btn-secondary mr-2"
					>
						Answer
					</button>
				</span>
			</li>
		));
	};
	
	toggle = () => {
		this.setState({ modal: !this.state.modal });
	};
	
	handleSubmit = item => {
		this.toggle();
		if ( item.id )
		{
			axios
				.put ( `http://localhost:8000/api/qas/${item.id}/`, item )
				.then ( res => this.refreshList () );
			return;
		}
		axios
			.post ( "http://localhost:8000/api/qas/", item )
			.then ( res => this.refreshList () );
	};
	
	handleDelete = item => {
		axios
			.delete ( `http://localhost:8000/api/qas/${item.id}` )
			.then ( red => this.refreshList () );
	};
	
	/* There should be the code for creating a new iteration but unfortunately
	 * I did not come so far in this time.*/
	createItem = () => {
		const item = { title: "", answer1: false, answer2: false, answer3: false };
		this.setState({ activeItem: item, modal: !this.state.modal });
	};
	
	editItem = item => {
		this.setState({ activeItem: item, modal: !this.state.modal });
	};
	
	render() {
		return (
			<main className="content">
				<h1 className="text-white text-uppercase text-center my-4">Question & Answer App</h1>
				<div className="row ">
					<div className="col-md-6 col-sm-10 mx-auto p-0">
						<div className="card p-3">
							<div className="">
								<button onClick = { this.createItem } className="btn btn-primary">
									New Iteration
								</button>
							</div>
							{this.renderTabList()}
							<ul className="list-group list-group-flush">
								{this.renderItems()}
							</ul>
						</div>
					</div>
				</div>
				{ this.state.modal ? (
					<Modal
						activeItem = { this.state.activeItem }
						toggle = { this.toggle }
						onSave = { this.handleSubmit }
					/>
				) : null }
			</main>
		);
	}
}
export default App;
