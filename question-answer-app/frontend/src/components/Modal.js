import React, { Component } from "react";
import {
	Button,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Form,
	FormGroup,
	Input,
	Label
} from "reactstrap";

export default class CustomModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			activeItem: this.props.activeItem
		};
	}
	
	handleChange = e => {
		let { name, value } = e.target;
		if (e.target.type === "checkbox") {
			value = e.target.checked;
		}
		const activeItem = { ...this.state.activeItem, [name]: value };
		this.setState({ activeItem });
	};
	
	render() {
		const { toggle, onSave } = this.props;
		return (
			<Modal isOpen={true} toggle={toggle}>
				<ModalHeader toggle={toggle}> Question</ModalHeader>
				<ModalBody>
					<Form>
						<FormGroup>
							<Label for="title">{ this.state.activeItem.title }</Label>
						</FormGroup>
						<FormGroup check>
							<Label for="answer1">
								<Input
									type="checkbox"
									name="answer1"
									checked={this.state.activeItem.answer1}
									onChange={this.handleChange}
								/>
								{ this.state.activeItem.lAnswer1 }
							</Label>
						</FormGroup>
						<FormGroup check>
							<Label for="answer2">
								<Input
									type="checkbox"
									name="answer2"
									checked={this.state.activeItem.answer2}
									onChange={this.handleChange}
								/>
								{ this.state.activeItem.lAnswer2 }
							</Label>
						</FormGroup>
						<FormGroup check>
							<Label for="answer3">
								<Input
									type="checkbox"
									name="answer3"
									checked={this.state.activeItem.answer3}
									onChange={this.handleChange}
								/>
								{ this.state.activeItem.lAnswer3 }
							</Label>
						</FormGroup>
					</Form>
				</ModalBody>
				<ModalFooter>
					<Button color="success" onClick={() => onSave(this.state.activeItem)}>
						Save
					</Button>
				</ModalFooter>
			</Modal>
		);
	}
}
