from django.db import models

# Create your models here.
class Qa ( models.Model ):
	title = models.CharField ( max_length = 120 )
	lAnswer1 = models.TextField ( default = "" )
	answer1 = models.BooleanField ( default = False )
	lAnswer2 = models.TextField ( default = "" )
	answer2 = models.BooleanField ( default = False )
	lAnswer3 = models.TextField ( default = "" )
	answer3 = models.BooleanField ( default = False )
	
	def _str_ ( self ):
		return self.title
