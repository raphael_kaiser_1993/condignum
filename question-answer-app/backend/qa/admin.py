from django.contrib import admin
from .models import Qa

class QaAdmin ( admin.ModelAdmin ):
	list_display = ( 'title', 'lAnswer1', 'answer1', 'lAnswer2', 'answer2', 'lAnswer3', 'answer3' )

# Register your models here.
admin.site.register ( Qa, QaAdmin )
