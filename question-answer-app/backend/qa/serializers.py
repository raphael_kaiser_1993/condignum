from rest_framework import serializers
from .models import Qa

class QaSerializer ( serializers.ModelSerializer ):
	class Meta:
		model = Qa
		fields = ( 'id', 'title', 'lAnswer1', 'answer1', 'lAnswer2', 'answer2', 'lAnswer3', 'answer3' )
