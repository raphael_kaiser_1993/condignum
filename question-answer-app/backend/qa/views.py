from django.shortcuts import render
from rest_framework import viewsets
from .serializers import QaSerializer
from .models import Qa

# Create your views here.
class QaView ( viewsets.ModelViewSet ):
	serializer_class = QaSerializer
	queryset = Qa.objects.all ()
